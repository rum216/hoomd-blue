HOOMD-blue
++++++++++

Welcome to the user documentation for HOOMD-blue!

The `hoomd-examples git repository <https://bitbucket.org/glotzer/hoomd-examples>`_ compliments this documentation with
a tutorial oriented introduction.

  * `View static examples at nbviewer.org <http://nbviewer.jupyter.org/github/joaander/hoomd-examples/blob/master/index.ipynb>`_
  * `Run the examples at mybinder.org <http://mybinder.org:/repo/joaander/hoomd-examples>`_ (CPU only).

.. toctree::
    :maxdepth: 2
    :caption: Concepts

    compiling
    command-line-options
    units
    box
    aniso
    nlist
    mpi
    autotuner
    restartable-jobs
    varperiod
    developer

.. toctree::
   :maxdepth: 3
   :caption: Stable python packages

   package-hoomd
   package-hpmc
   package-md
   package-dem

.. toctree::
   :maxdepth: 3
   :caption: Additional python packages

   package-cgcmm
   package-deprecated
   package-metal

.. toctree::
   :maxdepth: 3
   :caption: Reference

   license
   credits
   indices
